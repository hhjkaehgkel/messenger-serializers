#!/usr/bin/env bash

set -euo pipefail

source .env

is_in_docker() {
  grep -q docker /proc/1/cgroup || test "${CI:-false}" == "true"
}

docker_wrap() {
  image=$1
  shift
  if ! is_in_docker; then
    args=(
      --volume "$PWD:$PWD"
      --workdir "$PWD"
      --env "COMPOSER_HOME=$COMPOSER_HOME"
      --user "$(id -u):$(id -g)"
      --interactive
    )
    if [ -t 0 ]; then
      args+=("--tty")
    fi

    docker run --rm "${args[@]}" "$image" "$@"
  else
    exec "$@"
  fi
}

function composer_install() {
  if ! test -d "$1/vendor"; then
    interactive=
    if [ "${CI:-false}" == "true" ]; then
      interactive=--no-interaction
    fi
    bin/composer install --working-dir="$1" $interactive
  fi
}
